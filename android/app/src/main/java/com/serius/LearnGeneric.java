package com.serius;

import android.util.Log;

public class LearnGeneric<T> {
    private T number;


    public LearnGeneric(T number) {
        this.number = number;
    }

    public T getData(){
        Log.d(
                "CalendarModule",
                "Create event called with name: " + this.number
        );
        return this.number;
    }
    public  void setData( T data){
       this.number = data;
    }
}
