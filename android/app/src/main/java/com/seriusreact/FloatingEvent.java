package com.seriusreact;

import android.util.Log;
import com.facebook.react.bridge.ReactApplicationContext;
import com.facebook.react.bridge.ReactContextBaseJavaModule;
import com.facebook.react.bridge.ReactMethod;
import com.serius.LearnGeneric;


import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.view.View;
import android.widget.Toast;

public class FloatingEvent extends ReactContextBaseJavaModule {

  FloatingEvent(ReactApplicationContext context) {
    super(context);
  }

  public String getName() {
    return "FloatingEvent";
  }

  @ReactMethod
  public void createCalendarEvent() {
ReactApplicationContext context = getReactApplicationContext();

 Intent intent = new Intent(context, FloatingWidget.class);
 intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
 context.startService(intent);

    LearnGeneric<Integer> test = new LearnGeneric<>(8);
    testGeneric(test);
    System.out.println(test.getData());
  }
//  contravariant generic
  public static void testGeneric (LearnGeneric <? super Integer> number){
    number.setData(8);
  }
}

